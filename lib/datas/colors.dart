import 'package:flutter/material.dart';

List<Color> colors = [
  Colors.lightBlueAccent,
  Colors.pink[300],
  Colors.purpleAccent[400],
  Colors.deepPurple[300],
  Colors.tealAccent[700],
  Colors.amber[700],
  Colors.red[300],
];
