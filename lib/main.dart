import 'package:flutter/material.dart';
import 'package:kazlingua8/pages/MainPage.dart';
import 'package:kazlingua8/pages/AboutPage.dart';
import 'package:kazlingua8/pages/GamePage.dart';
import 'package:kazlingua8/pages/PartPage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kazlingua8/models/MainModel.dart';

void main() => runApp(
      new ScopedModel<MainModel>(
        model: MainModel(),
        child: MaterialApp(
//      initialRoute: '/game',
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => MainPage(),
            '/about': (context) => AboutPage(),
            '/part': (context) => PartPage(),
            '/game': (context) => GamePage(),
          },
        ),
      ),
    );
